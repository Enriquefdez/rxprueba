package com.example.rxjavatutorial

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
//import com.google.android.gms.ads.AdRequest
//import com.google.android.gms.ads.AdView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CronometroActivity : AppCompatActivity() {

    lateinit var timer : Disposable
    lateinit var observable : Observable<Long>
    lateinit var mediaPlayer : MediaPlayer
   // lateinit var mAdView : AdView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cronometro)
        var min = 0
        var hora = 0
        var sec = 0
    mediaPlayer = MediaPlayer.create(applicationContext, R.raw.alarma_5)

       var texto = findViewById<TextView>(R.id.displayTimerOnScreen)
        var cinco = findViewById<Button>(R.id.btn5)
        var treinta = findViewById<Button>(R.id.btn30)
        var sesenta = findViewById<Button>(R.id.btn60)

        cinco.setOnClickListener {
            min = 5
            sec = 60

            func(min, sec, hora, texto, cinco, treinta, sesenta)

        }
        treinta.setOnClickListener {
            min = 30
            sec = 60

            func(min, sec, hora, texto, cinco, treinta, sesenta)
        }

        sesenta.setOnClickListener {
            min = 60
            sec = 60
            hora = 1

            func(min, sec, hora, texto, cinco, treinta, sesenta)
        }


    }

    private fun mostrar(cinco: Button, treinta: Button, sesenta: Button) {
        cinco.visibility = View.VISIBLE
        treinta.visibility = View.VISIBLE
        sesenta.visibility = View.VISIBLE
    }

    private fun esconder(cinco: Button, treinta: Button, sesenta: Button) {
        cinco.visibility = View.GONE
        treinta.visibility = View.GONE
        sesenta.visibility = View.GONE
    }

    private fun func(min : Int, sec : Int, hora: Int, texto : TextView, cinco: Button, treinta: Button, sesenta: Button) {
        observable = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
        var segun = sec
        var minu = min
        var hor = hora
        timer = observable.subscribe({
            esconder(cinco,treinta,sesenta)
            hor = 0
            if(minu == 60){
                minu --
            }
            if(segun == 60){
                minu--
            }
            segun--
            if(segun == 0){

                if(minu == 0 && segun == 0 && hor == 0) {
                    timer.dispose()
                    texto.text = getString(R.string.texto_crono_fin)

                    mediaPlayer.start()
                    mostrar(cinco,treinta,sesenta)

                }else {
                    minu--
                    segun = 59
                    "$hor : $minu : $segun".also { texto.text = it }

                }


            }else{
                "$hor : $minu : $segun".also { texto.text = it }
            }



        }, { })

    }
}
