package com.example.rxjavatutorial

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rxjavatutorial.adapter.PostsAdapter
import com.example.rxjavatutorial.model.Posts
import com.example.rxjavatutorial.retrofit.MyAPI
import com.example.rxjavatutorial.retrofit.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

class MainClassApi : AppCompatActivity() {
    lateinit var myAPI: MyAPI
    lateinit var recycler_posts : RecyclerView
    var compositeDisposable : CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var retrofit : Retrofit? = RetrofitClient.getInstance()
        if (retrofit != null) {
            myAPI = retrofit.create(MyAPI::class.java)
        }

        recycler_posts = findViewById(R.id.recycler_posts)
        recycler_posts.setHasFixedSize(true)

        recycler_posts.layoutManager = LinearLayoutManager(this)

        fetchData()
    }

    private fun fetchData() {
        compositeDisposable.add(myAPI.getPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(Consumer {
                displayData(it)
            }))
    }

    private fun displayData(it: List<Posts>) {
        var adapter : PostsAdapter = PostsAdapter(this, it)
        recycler_posts.adapter = adapter

    }

    override fun onStop() {
        compositeDisposable.dispose()
        super.onStop()
    }
}