package com.example.rxjavatutorial.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rxjavatutorial.R
import com.example.rxjavatutorial.model.Posts
import java.lang.StringBuilder

class PostsAdapter : RecyclerView.Adapter<PostViewHolder> {
    var context: Context
    var postsList : List<Posts>
    constructor(context: Context, postsList: List<Posts>){
        this.context = context
        this.postsList = postsList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        var view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_layout, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.txtAuthor.text = postsList.get(position).userId.toString()
        holder.txtTitle.text = postsList.get(position).title
        holder.txtContent.text = StringBuilder(postsList.get(position).body.substring(0,20)).append("...").toString()
    }

    override fun getItemCount(): Int {
        return postsList.size
    }
}