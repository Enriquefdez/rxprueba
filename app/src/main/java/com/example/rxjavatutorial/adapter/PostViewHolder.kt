package com.example.rxjavatutorial.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rxjavatutorial.R


class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtTitle : TextView = itemView.findViewById(R.id.txtTitle)
    var txtContent : TextView = itemView.findViewById(R.id.txtContent)
    var txtAuthor : TextView = itemView.findViewById(R.id.txtAuthor)
}