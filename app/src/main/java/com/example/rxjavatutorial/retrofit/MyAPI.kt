package com.example.rxjavatutorial.retrofit

import com.example.rxjavatutorial.model.Posts
import io.reactivex.Observable
import retrofit2.http.GET


interface MyAPI {
    @GET("posts")
    fun getPosts() : Observable<List<Posts>>


}