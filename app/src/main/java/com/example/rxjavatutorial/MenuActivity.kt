package com.example.rxjavatutorial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val contador = findViewById<Button>(R.id.btn_contador)
        val json = findViewById<Button>(R.id.btn_json)

        contador.setOnClickListener {
            startActivity(Intent(this, CronometroActivity::class.java))
        }

        json.setOnClickListener {
            startActivity(Intent(this, MainClassApi::class.java))
        }

    }
}